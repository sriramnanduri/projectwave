-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ats
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `categorymaster`
--

LOCK TABLES `categorymaster` WRITE;
/*!40000 ALTER TABLE `categorymaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quesansmaster`
--

LOCK TABLES `quesansmaster` WRITE;
/*!40000 ALTER TABLE `quesansmaster` DISABLE KEYS */;
INSERT INTO `quesansmaster` VALUES (1,NULL,'Please look into the video and identify if the wave is a construction wave or a destruction wave.',NULL,NULL,'CM1.mov','[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','1',NULL,NULL,NULL,NULL),(2,NULL,'Please look at the image and identify if the wave is a construction wave or a destruction wave.',NULL,'CI1.jpg',NULL,'[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','1',NULL,NULL,NULL,NULL),(3,NULL,'Please look into the video and identify if the wave is a construction wave or a destruction wave.',NULL,NULL,'CM2.MOV','[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','1',NULL,NULL,NULL,NULL),(4,NULL,'Please look at the image and identify if the wave is a construction wave or a destruction wave.',NULL,'CI2.jpg',NULL,'[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','1',NULL,NULL,NULL,NULL),(5,NULL,'Please look at the image and identify if the wave is a construction wave or a destruction wave.',NULL,'CI3.PNG',NULL,'[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','1',NULL,NULL,NULL,NULL),(6,NULL,'Please look at the image and identify if the wave is a construction wave or a destruction wave.',NULL,'DI1.jpg','','[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','2',NULL,NULL,NULL,NULL),(7,NULL,'Please look at the image and identify if the wave is a construction wave or a destruction wave.',NULL,'DI2.PNG','','[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','2',NULL,NULL,NULL,NULL),(8,NULL,'Please look into the video and identify if the wave is a construction wave or a destruction wave.',NULL,NULL,'DM1.mov','[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','2',NULL,NULL,NULL,NULL),(9,NULL,'Please look at the image and identify if the wave is a construction wave or a destruction wave.',NULL,'DI3.JPG',NULL,'[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','2',NULL,NULL,NULL,NULL),(10,NULL,'Please look into the video and identify if the wave is a construction wave or a destruction wave.',NULL,NULL,'DM2.MOV','[{\"id\": 1, \"answer\": \"Construction Wave\"}, {\"id\": 2, \"answer\": \"Destruction Wave\"}, {\"id\": 3, \"answer\": \"Hybrid\"}, {\"id\": 4, \"answer\": \"None of the above\"}]','2',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `quesansmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` VALUES (1,NULL,0,0,0,'2017-12-18 06:21:53',0,10,'[\"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 12, 8, 16, 14, 2, 3, 6]'),(2,NULL,0,1,0,'2017-12-20 06:12:07',1,9,'[\"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 8, 2, 3, 6, 5, 10, 9]'),(3,NULL,0,2,0,'2017-12-20 06:12:09',2,8,'[\"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 8, 2, 3, 6, 5, 10, 9]'),(4,NULL,0,3,0,'2017-12-20 06:12:09',3,7,'[\"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 8, 2, 3, 6, 5, 10, 9]'),(5,NULL,0,4,0,'2017-12-20 06:12:09',4,6,'[\"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"2\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 8, 2, 3, 6, 5, 10, 9]'),(6,'1',0,1,0,'2017-12-20 06:12:45',1,9,'[\"3\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 8, 2, 3, 6, 5, 10, 9]'),(7,'1',2,3,2,'2017-12-21 03:45:01',5,5,'[\"1\", \"3\", \"1\", \"3\", \"1\", \"\", \"\", \"\", \"\", \"\"]','[1, 7, 4, 8, 2, 3, 6, 5, 10, 9]');
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usermaster`
--

LOCK TABLES `usermaster` WRITE;
/*!40000 ALTER TABLE `usermaster` DISABLE KEYS */;
INSERT INTO `usermaster` VALUES (1,'test','test','Test','Test','123','123@gmail.com',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `usermaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'ats'
--

--
-- Dumping routines for database 'ats'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-21  9:15:52

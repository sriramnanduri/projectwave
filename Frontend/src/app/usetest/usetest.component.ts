import { Component, ViewChild, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { forEach } from '@angular/router/src/utils/collection';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usetest',
  templateUrl: './usetest.component.html',
  styleUrls: ['./usetest.component.css']
})
export class UsetestComponent implements OnInit {
  // modelanswers: any[] = ["","","","","","","","","","",""];
  modelanswers: any[] = [""];
  modelanswersArray: any = [];
  min = 9;
  sec = 60;
  notanswered: number = 0;
  wrongAns: number = 0;
  result = {
    attempted: 0,
    scoreObtained: 0
  }
  queids: any = [];
  hideForwardButton: boolean;
  hideFinalResult: boolean;
  scoreObtained: number = 0;
  finalResult: number;
  idx: number;
  quesAnsIDs: any = {};
  answerArrayList: any = [];
  disabledBackButton: boolean;
  answerGroup: any = [];
  QuestionArray: any = [];
  questionArray: any = [];
  AnswerArray: any = [];
  question: any = {};
  incrementID: number;
  bindedValue: number;
  resultValues: any = {};
  timer1: any;
  timer2: any;
  userID = this.commonservice.globalID;

  constructor(private commonservice: CommonService, private router: Router) {
    this.timer1 = setInterval(() => { this.min == 0 ? this.submitTest() : --this.min }, 60000);
    this.timer2 = setInterval(() => { this.sec = this.sec == 1 ? 60 : --this.sec }, 1000);
  }

  answerID: number;
  @ViewChild('videoPlayer') videoplayer: any;
  toggleVideo() {
    // alert("pLAYING VIDEO ")
    this.videoplayer.nativeElement.play();
  }

  ngOnInit() {
    this.hideForwardButton = false;
    this.disabledBackButton = true;
    // this.hideFinalResult = true;
    // alert(this.commonservice.globalID);
    this.incrementID = 1;
    this.commonservice.getQuestionAndAnswers((returnObject) => {
      for (let i = 0; i < returnObject.length; i++) {
        this.QuestionArray.push(returnObject[i]);
        this.modelanswers.push("");
        this.queids[i] = this.QuestionArray[i].id;
      }
      this.questionArray = this.QuestionArray;
      this.question = this.questionArray[0];
    })
  }

  forwardQuesion(i, questionID) {
    if (i < 10) {
      this.question = this.QuestionArray[i];
    }
    if (this.incrementID < 10) {
      this.incrementID = this.incrementID + 1;
      this.disabledBackButton = (this.incrementID == 1) ? true : false;
      this.hideForwardButton = (this.incrementID == 10) ? true : false;
    }
  }

  backwardQuesion(i) {
    if (i <= 10) {
      this.question = this.QuestionArray[(i - 2)];
    }
    if (this.incrementID > 1) {
      this.incrementID = this.incrementID - 1;
      this.disabledBackButton = (this.incrementID == 1) ? true : false;
      this.hideForwardButton = (this.incrementID == 10) ? true : false;
    }
  }

  submitTest() {
    clearInterval(this.timer1);
    clearInterval(this.timer2);
    for (let i = 0; i < this.questionArray.length; i++) {
      if (this.modelanswers[i + 1] != '') {
        ++this.result.attempted;
        if (this.QuestionArray[i].correctanswer == this.modelanswers[i + 1]) {
          ++this.result.scoreObtained;
        }
        else {
          ++this.wrongAns;
        }
      }
      else {

      }
      this.modelanswersArray.push(this.modelanswers[i + 1])
    }
    
    this.resultValues = {
      score: this.result.scoreObtained,
      correctanswers: this.result.scoreObtained,
      wronganswers: this.wrongAns,
      userid: this.userID,
      unattempted: (10 - this.result.attempted),
      attempted: this.result.attempted,
      useranswer: this.modelanswersArray,
      queid: this.queids
    }
    this.hideFinalResult = false;
    this.commonservice.insertResult(this.resultValues, (finalData) => {
      this.router.navigate(['/results', this.userID]);
    })
  }

  backNextFun(incID) {
    if (incID > 1 && incID < 10) {
      this.hideForwardButton = false;
      this.disabledBackButton = false;
    }
    else if (incID == 10) {
      this.hideForwardButton = true;
      this.disabledBackButton = false;
    }
    else if (incID == 1) {
      this.hideForwardButton = false;
      this.disabledBackButton = true;
    }
  }

  getStyle(i) {
    if (this.modelanswers[i + 1] == "") {
      return "btn btn-info";
    } else {
      return "btn btn-success";
    }
  }
  getPath(pathI) {
    return "http://localhost:8080/fp/api/userprofile/profileimage/" + pathI;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsetestComponent } from './usetest.component';

describe('UsetestComponent', () => {
  let component: UsetestComponent;
  let fixture: ComponentFixture<UsetestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsetestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsetestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

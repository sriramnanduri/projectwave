import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { UsetestComponent } from './usetest/usetest.component';
import { ResultsComponent } from './results/results.component';

import { CommonService } from './common.service';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderloginComponent } from './headerlogin/headerlogin.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent,pathMatch:'full'},
  { path: 'aboutus', component: AboutusComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'login', component: LoginComponent },
  { path:'usetest',component:UsetestComponent },
  { path:'results/:id',component:ResultsComponent },
  {path:"**",redirectTo:''}]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutusComponent,
    LoginComponent,
    RegistrationComponent,
    UsetestComponent,
    ResultsComponent,
    HeaderComponent,
    FooterComponent,
    HeaderloginComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }

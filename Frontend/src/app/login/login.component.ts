import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';
import { AppComponent } from '../app.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hideEmpty: boolean = true;
  loginForm;
  loginObject: any = {};
  result: any = {};
  hideErrorLogin = true;


  constructor(private router: Router, private LForm: FormBuilder, private commonservice: CommonService, private appcomponent: AppComponent) {
    this.loginForm = this.LForm.group({})
  }

  ngOnInit() {
    this.appcomponent.hideHeaderLogin = true;
    this.appcomponent.hideHeader = false;
  }

  loginUser = function (data) {
    data.preventDefault();
    this.loginObject = {
      username: data.target.elements[0].value,
      password: data.target.elements[1].value
    }
    if (this.loginObject.username == '' || this.loginObject.password == '') {
      this.hideEmpty = false;
      this.hideErrorLogin = true;
    }
    else {
      this.commonservice.getEmailPass(this.loginObject, (result) => {
        if (result.data.length !== 0) {
          this.router.navigate(['/usetest']);
          this.appcomponent.hideHeaderLogin = false;
          this.appcomponent.hideHeader = true;
        }
        else
          this.hideErrorLogin = false;
      })
    }
  }


  hideError = function () {
    this.hideErrorLogin = true;
    this.hideEmpty = true;

  }

}
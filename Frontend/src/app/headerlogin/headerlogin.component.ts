import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { forEach } from '@angular/router/src/utils/collection';
import { Router } from '@angular/router';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-headerlogin',
  templateUrl: './headerlogin.component.html',
  styleUrls: ['./headerlogin.component.css']
})
export class HeaderloginComponent implements OnInit {

  constructor(private commonservice:CommonService,private router:Router, private appcomponent:AppComponent) { }

  ngOnInit() {
    // if(this.commonservice.globalID==0) this.router.navigate(['/login']);
  }

  logout(){
    // this.commonservice.globalID=0;
    this.router.navigate(['/login']);
    this.appcomponent.hideHeaderLogin=true;
    this.appcomponent.hideHeader=false;
    this.appcomponent.hideFooter=false;
 
  }

}

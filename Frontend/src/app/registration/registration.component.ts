import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})

export class RegistrationComponent implements OnInit {
  RegSuccess: boolean = true;
  errorInPassword: boolean;
  hideLogoff = true;;
  firstname: any;
  lastname: any;
  mobile: any;
  emailid: any;
  username: any;
  password: any;
  confirmpassword: any;
  loginform: FormGroup;
  data: {};
  roles: {};
  getObj: any = [];
  roles1: any = [];

  constructor(private login: FormBuilder, private commonService: CommonService, private router: Router) {
    this.loginform = login.group({
      firstname: [null, [Validators.required, Validators.minLength(3)]],
      lastname: [null, [Validators.required, Validators.minLength(3)]],
      mobile: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("[0-9]*")]],
      password: [null, [Validators.required]],
      username: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(8)]],
      confirmpassword: [null, Validators.required],
      emailid: [null, [Validators.required, Validators.email]]
    })
  }

  RegistrationDtls(data) {
    this.commonService.RegistrationDtls(data, (response) => {
      this.RegSuccess = false;
    })
  };

  ngOnInit() {
    this.errorInPassword = true;
  }
  
  matchPassword(CPassword) {
    if (this.password == CPassword) {
      this.errorInPassword = true;
    }
    else {
      this.errorInPassword = false;
    }
  }
};

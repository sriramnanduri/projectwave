import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';


@Injectable()
export class CommonService {
  public globalID: number;
  public answerKeyArray: any = [];
  constructor(private http: HttpClient) { }


  getEmailPass = function (data, callback) {
    this.http.post('http://localhost:8080/fp/api/userprofile/getEmailPass', data).subscribe(
      (returnData) => {
        if (returnData.data.length !== 0)
          this.globalID = returnData.data[0].id;
        callback(returnData);
      }
    )
  }

  getQuestionAndAnswers = function (callback) {
    this.answerKeyArray = [];
    this.http.get('http://localhost:8080/fp/api/userprofile/getQuestionandAnswers').subscribe(
      (returnData) => {
        for (let i = 0; i < returnData.length; i++) {
          this.answerKeyArray.push(returnData[i].correctanswer);
        }
        callback(returnData);
      })
  }


  RegistrationDtls(data, callback) {
    return this.http.post("http://localhost:8080/fp/api/userprofile/userRegistration", data).subscribe((returnData) => {
      callback(returnData);
    })
  }

  insertResult(data, callback) {
    this.http.post('http://localhost:8080/fp/api/userprofile/insertResult', data).subscribe(
      (returnData) => {
        callback(returnData);
      }
    )
  }

  getResults(id, callback) {
    this.http.get('http://localhost:8080/fp/api/userprofile/getResult/' + id).subscribe(
      (returnData) => {
        callback(returnData);
      }
    )
  }

}

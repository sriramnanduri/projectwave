import { Component, OnInit, NgModule } from '@angular/core';
import { CommonService } from '../common.service';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})

export class ResultsComponent implements OnInit {
  id: any = this.commonservice.globalID;
  answerKeyArrayList: any = this.commonservice.answerKeyArray
  array: any = [];
  AAnswerObject: any = [];
  UAnswerObject: any = [];
  UAnswerArray: any = [];
  AAnswerArray: any = [];
  actualAnswers: any = [];
  // answerAray:any=this.usetest.userAnswerList;
  resultObject: any = {};


  constructor(private commonservice: CommonService) { }

  ngOnInit() {
    this.commonservice.getResults(this.id, (results) => {
      this.resultObject = {
        maxMarks: 10,
        Score: results[results.length - 1].score,
        maxQuestions: 10,
        AttemptedQuestions: results[results.length - 1].attempted,
        CorrectAnswers: results[results.length - 1].correctanswers,
        userAnswers: results[results.length - 1].useranswer
      }
    })
  }

}


"use strict"

function emailTemplates() {
  
    this.getTemplate = function(templateName, params){ 

  this.otpTemplate = '<div style="width:600px; padding: 0; margin:0 auto;"><table cellpadding="0" cellspacing="0" border="0" width="100%">     <tr><td><div style=" padding:10px; text-align:left;border-bottom:4px solid #210768">      <a style="font-size: 30px;text-align: center;color: #929292 !important;" >Fund<span style="color: #3391cc;">Pitch</span></a> </div></td></tr><tr> <td><div style="padding:10px"><p style="font-size:14px;">Welcome to FundPitch,</p>  <p style="font-size:12px; font-weight:normal; padding-top: 20px">This is an email for Account Verification <p style="font-size:12px; font-weight:normal; padding-top: 15px"><b>Your Verification Code is: ##OTP##</b></p>  <p style="font-size:12px; font-weight:normal; padding-top: 15px">If you have any questions regarding your account, please contact us at <a href="mailto://contact@fundpitch.com">contact@fundpitch.com</a></p> <p style="font-size:12px; font-weight:normal; padding-top: 25px">Warm Regards,</p> <p style="font-size:12px; font-weight:bold; padding: 0px 0 40px 0">FundPitch Admin</p> </div></td></tr></table></div>';

    this.thanksRegister = '<div style="width:600px; padding: 0; margin:0 auto;"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td><div style=" padding:10px; text-align:left;border-bottom:4px solid #210768">      <a style="font-size: 30px;text-align: center;color: #929292 !important;" >Fund<span style="color: #3391cc;">Pitch</span></a> </div></td></tr><tr> <td><div style="padding:10px"><p style="font-size:14px;">Welcome to FundPitch,</p>  <p style="font-size:12px; font-weight:normal; padding-top: 20px">Thank you for registering with <b> FundPitch</b>. We are currently validating your account. This process typically takes 1 to 2 business days. </p>     <p style="font-size:12px; font-weight:normal; padding-top: 15px">We will notify you regarding the activation of your account, as soon as the verification process is complete.</p><p style="font-size:12px; font-weight:normal; padding-top: 15px">If you have any questions regarding your registration, please contact us at <a href="mailto://contact@fundpitch.com">here</a></p><p style="font-size:12px; font-weight:normal; padding-top: 25px">Warm Regards,</p><p style="font-size:12px; font-weight:bold; padding: 0px 0 40px 0">FundPitch Admin</p></div></td></tr></table></div>';

    this.viewProjectByRegisteredUser = '<div style="width:600px; padding: 0; margin:0 auto;"><table cellpadding="0" cellspacing="0" border="0" width="100%">     <tr><td><div style=" padding:10px; text-align:left;border-bottom:4px solid #210768">      <a style="font-size: 30px;text-align: center;color: #929292 !important;" >Fund<span style="color: #3391cc;">Pitch</span></a> </div></td></tr><tr>         <td><div style="padding:10px"><p style="font-size:14px;">Hi ##name##</p>     <div style="padding:10px"><p style="font-size:14px;">Greetings from FundPitch!</p>             <p style="font-size:12px; font-weight:normal; padding-top: 20px">You have been invited to participate in a project listed on the FundPitch platform. There is a new project created with the below summary:<br/><br/><p style="font-size:12px; font-weight:normal; padding-top: 20px">##summary##</p><br/><p style="font-size:12px; font-weight:normal; padding-top: 20px">Inviter&#39;s comments: ##comments## </p><p style="font-size:12px; font-weight:normal; padding-top: 20px">View Project - <p style="font-size:12px; font-weight:normal; padding-top: 15px"> URL: ##url##</p> <p style="font-size:12px; font-weight:normal; padding-top: 20px">Signup - <p style="font-size:12px; font-weight:normal; padding-top: 15px"> URL: <a href="wwww.fundpitch.com">FundPitch</a></p>           <p style="font-size:12px; font-weight:normal; padding-top: 15px">If you have any questions regarding the project, please contact us at <a href="mailto://contact@fundpitch.com">here</a></p>                     <p style="font-size:12px; font-weight:normal; padding-top: 25px">Warm Regards,</p>         <p style="font-size:12px; font-weight:bold; padding: 0px 0 40px 0">FundPitch Admin</p>         </div></td></tr></table></div>';

    this.viewProjectByExternalUser = '<div style="width:600px; padding: 0; margin:0 auto;"><table cellpadding="0" cellspacing="0" border="0" width="100%">     <tr><td><div style=" padding:10px; text-align:left;border-bottom:4px solid #210768">      <a style="font-size: 30px;text-align: center;color: #929292 !important;" >Fund<span style="color: #3391cc;">Pitch</span></a> </div></td></tr><tr>         <td><div style="padding:10px"><p style="font-size:14px;">Hi ##name##</p>    <div style="padding:10px"><p style="font-size:14px;">Welcome to FundPitch,</p>             <p style="font-size:12px; font-weight:normal; padding-top: 20px">You have been invited to FundPitch Platform for a new investment. Below is the summary of the project:<br/><br/><p style="font-size:12px; font-weight:normal; padding-top: 20px">##summary##</p><br/><p style="font-size:12px; font-weight:normal; padding-top: 20px">Following are the comments on this project: <br/> ##comments## </p><p style="font-size:12px; font-weight:normal; padding-top: 20px">This is the notification to view the project. You need to register in order to view the project details. Please visit <a href="www.fundpitch.com">FundPitch</a> website to register yourself. <p style="font-size:12px; font-weight:normal; padding-top: 15px">If you have any questions regarding the project, please contact us at <a href="mailto://contact@fundpitch.com">here</a></p>                     <p style="font-size:12px; font-weight:normal; padding-top: 25px">Warm Regards,</p>         <p style="font-size:12px; font-weight:bold; padding: 0px 0 40px 0">FundPitch Admin</p>         </div></td></tr></table></div>';

    this.approveUser = '<div style="width:600px; padding: 0; margin:0 auto;"><table cellpadding="0" cellspacing="0" border="0" width="100%"> <tr><td><div style=" padding:10px; text-align:left;border-bottom:4px solid #210768">      <a style="font-size: 30px;text-align: center;color: #929292 !important;" >Fund<span style="color: #3391cc;">Pitch</span></a> </div></td></tr><tr> <td><div style="padding:10px"><p style="font-size:14px;">Welcome to FundPitch,</p><p style="font-size:12px; font-weight:normal; padding-top: 15px"><b>You have been approved by fundpitch admin.Your user id and password were generated.<div>Login Id : ##email##</div><div> Password : ##password## </div> </b></p><p style="font-size:12px; font-weight:normal; padding-top: 15px">If you have any questions regarding your account, please contact us at <a target="_blank" href="http://fundpitch.com">fundpitch</a></p> <p style="font-size:12px; font-weight:normal; padding-top: 25px">Thanks,</p> <p style="font-size:14px; font-weight:bold; padding: 0px 0 40px 0">FundPitch</p>         </div></td></tr></table></div>'


        if(templateName === 'otpTemplate'){
            if(params.otpNumber !== undefined){
                this.otpTemplate = this.otpTemplate.replace("##OTP##", params.otpNumber);
            }
            return this.otpTemplate;
        }
        else if(templateName === 'thanksRegister'){
            return this.thanksRegister;
        }
        else if(templateName === 'projectCreated'){
            return this.projectCreated;
        }
        else if(templateName === 'viewProjectByRegisteredUser'){
            if(params.name !== undefined && params.comments && params.viewProjectURL){
                this.viewProjectByRegisteredUser = this.viewProjectByRegisteredUser.replace("##name##",params.name);
                this.viewProjectByRegisteredUser = this.viewProjectByRegisteredUser.replace("##comments##",params.comments);
                this.viewProjectByRegisteredUser = this.viewProjectByRegisteredUser.replace("##url##",params.viewProjectURL);
                this.viewProjectByRegisteredUser = this.viewProjectByRegisteredUser.replace("##summary##",params.summary);
            }
            return this.viewProjectByRegisteredUser;
        }
        else if(templateName === 'viewProjectByExternalUser'){
            if(params.name !== undefined && params.comments && params.viewProjectURL){
                this.viewProjectByExternalUser = this.viewProjectByExternalUser.replace("##name##",params.name);
                this.viewProjectByExternalUser = this.viewProjectByExternalUser.replace("##comments##",params.comments);
                this.viewProjectByExternalUser = this.viewProjectByExternalUser.replace("##url##",params.viewProjectURL);
                this.viewProjectByExternalUser = this.viewProjectByExternalUser.replace("##summary##",params.summary);
            }
            return this.viewProjectByExternalUser;
        }
        else if(templateName === 'approveUser'){
             if(params.email !== undefined){
               
                this.approveUser = this.approveUser.replace("##password##", params.password);
                this.approveUser = this.approveUser.replace("##email##", params.email);
            }
            console.log(this.approveUser);
            return this.approveUser;
        }
    }
}

emailTemplates = new emailTemplates();

module.exports = emailTemplates;
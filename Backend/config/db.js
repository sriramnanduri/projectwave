"use stric"

var Sequelize = require('sequelize');
var config = require('./config');

var db = null;

module.exports.db = function () {
    if (db === null) {
        db = new Sequelize(config.serverConfig.database.dbName, config.serverConfig.database.user, config.serverConfig.database.password, {
            host: config.serverConfig.database.host,
            port: 3306,
            dialect: 'mysql',
            dialectOptions: {
                multipleStatements: true
            },
            pool: {
                max: 60,
                min: 0,
                idle: 7200
            }
        });
        //Checking connection status

        db.authenticate().then(function (err) {
            console.log(err);
            if (err) {
                console.log('There is connection in ERROR');
            } else {
                console.log('Connection has been established successfully to DB: ' + config.serverConfig.database.dbName + ' on ' + config.serverConfig.database.host + ':3306 with user: ' + config.serverConfig.database.user);

            }
        });
    }
    return db;
};


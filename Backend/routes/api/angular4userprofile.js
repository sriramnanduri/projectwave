"use strict"
var express = require('express'),
    router = express.Router(),
    path = require('path'),
    fs = require("fs"),
    userprofileService = require('../../services/angular4userprofileService');
    

    router.post('/getEmailPass', function (req,res) {   
        userprofileService.getEmailPassword(req.body,function (response) {
            res.send(response);     
        })
    });

        
    router.get('/getQuestionandAnswers', function (req,res) { 
        userprofileService.getQuestionandAnswers(function (response) {
            res.send(response.getdata);     
        })
    });

    router.post('/userRegistration', function (req,res) {
        userprofileService.userRegistration(req.body,function (response) {
            res.send(response.data);    
        })
    });

    router.post('/insertResult', function (req,res) {
        userprofileService.insertResults(req.body,function (response) {
            res.send(response.data);    
        })
    });

    router.get('/getResult/:id', function (req,res) {
        userprofileService.getResults(req.params.id,function (response) {
            res.send(response.getdata);    
        })
    });

    router.get("/profileimage/:imagename", function (req, res) {
        console.log('profileimage...'+req.params.imagename);
        var file = path.join(__dirname, '../../img/profile/') + req.params.imagename;
        if (fs.existsSync(file)) {
        var filestream = fs.createReadStream(file);
        filestream.pipe(res);
        }    
    });

module.exports = router;


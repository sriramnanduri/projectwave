"use strict"

var express = require('express'),
    router = express.Router(),

    userProfile_api = require('./angular4userprofile');

router.use('/userProfile', userProfile_api);
module.exports = router;
/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exam', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userid: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    quesansid: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    useranswer: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    examresult: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    createdBy: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'exam'
  });
};

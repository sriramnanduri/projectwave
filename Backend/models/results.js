/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('results', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userid: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    correctanswers: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    wronganswers: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    score: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    datetime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    attempted: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    unattempted: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    useranswer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    queid: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    tableName: 'results',
    timestamps:false
  });
};

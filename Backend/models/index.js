var sequelize = require('../config/db').db();

// load models
var models = [
 'categorymaster','quesansmaster','exam','usermaster','userprofile','results'
];

models.forEach(function (model) {
  module.exports[model] = sequelize.import(__dirname + '/' + model);
});

// export connection
module.exports.sequelize = sequelize;

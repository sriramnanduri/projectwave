/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('quesansmaster', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    categoryid: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    question: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    contentPath: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    quesimage: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    quesvideo: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    answer: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    correctanswer: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    createdBy: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'quesansmaster'
  });
};


"use strict"

let userprofile = require('../models').usermaster;
let questionAndAnswers = require('../models').quesansmaster;
let resultsData = require('../models').results;
var Sequelize = require('sequelize');

function userProfile() {


    this.getEmailPassword = function (data, callback) {
        userprofile.findAll({
            attributes: ['id', 'username', 'password'],
            where: {
                username: data.username,
                password: data.password
            }
        }).then(function (response) {
            callback({ data: response, status: 0 });
        })
            .catch(function (err) {
                callback({ status: 1, data: err });
            })

    };


    this.getQuestionandAnswers = function (callback) {
        questionAndAnswers.findAll({
            order: [
                [Sequelize.fn('RAND', '')]
              ],
            limit: 10,
            attributes: ['id', 'question', 'answer', 'correctanswer','quesimage','quesvideo'],
        })
            .then(function (result) {
                if (result) {
                    callback({ status: 0, getdata: result });
                } else {
                    callback({ status: 1, errMessage: 'Getting data  Err' });
                }
            })
    };

    this.userRegistration = function (data, callback) {
        userprofile.build(data).save().then(function (response) {
            callback({ status: 0, data: response });
        });
    };

    this.insertResults = function (data, callback) {
        let mainObject = {
            datetime: new Date(),
            score: data.score,
            correctanswers: data.correctanswers,
            wronganswers: data.wronganswers,
            userid: data.userid,
            attempted: data.attempted,
            unattempted: data.unattempted,
            useranswer: data.useranswer,
            queid:data.queid
        }
        resultsData.build(mainObject).save().then(function (response) {
            callback({ status: 0, data: response });
        });
    };

    this.getResults = function (userID, callback) {
        resultsData.findAll({
            where: {
                userid: userID
            }

        })
            .then(function (result) {
                if (result) {

                    callback({ status: 0, getdata: result });
                } else {
                    callback({ status: 1, errMessage: 'Getting data  Err' });
                }
            })
    };


}

userProfile = new userProfile();

module.exports = userProfile;

